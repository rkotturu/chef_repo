#
# Cookbook:: first
# Recipe:: default
#
# Copyright:: 2018, The Authors, All Rights Reserved.
log "Welcome to Chef, ROHIT" do
  level :info
end
package 'nginx' do
  action :install
end

service 'nginx' do
  action [ :enable, :start ]
end

package 'telnet' do
	action :install
end

package 'bind-utils' do
	action :install
end

package 'httpd' do
        action :install
end
